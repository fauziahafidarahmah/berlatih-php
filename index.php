<?php

class Animal {
    public $legs = 2;
    public $cold_blooded = "false";
    public function __construct($name)
    {
        $this->name = $name;
    }
}


$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->cold_blooded; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

class Frog extends Animal 
{
    public function jump()
    {
        echo "hop hop";
    }
}

class Ape extends Animal
{
    public function yell()
    {
        echo "Auooo";
    }
}

// index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell() ;// "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"


?>