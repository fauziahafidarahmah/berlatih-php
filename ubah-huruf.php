<?php
function ubah_huruf($string){
    $alphas = range('a', 'z');
    $result = '';
    for ($i=0; $i<strlen($string);$i++){
        $ni = array_search($string[$i], $alphas);
        $result .= $alphas[($ni+1)];
    }
    return $result . "<br>";
//kode di sini
};

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>